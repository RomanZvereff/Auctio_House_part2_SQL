package com.zrv.auction;

import java.sql.*;
import java.util.ArrayList;

public class Seller {

    private String id;
    private String sellerFirstName;
    private String sellerLastName;
    private String sellerPhoneNumber;

    public String getId(String id) {
        return id;
    }

    public String getSellerFirstName(String sellerFirstName) {
        return sellerFirstName;
    }

    public String getSellerLastName(String sellerLastName) {
        return sellerLastName;
    }

    public String getSellerPhoneNumber(String sellerPhoneNumber) {
        return sellerPhoneNumber;
    }

    public ArrayList<Seller> getValuesSeller() throws SQLException {


        ArrayList<Seller> seller = new ArrayList<>();

        Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/auction", "root", "123456");

        Statement statement = connection.createStatement();

        String script = "SELECT * FROM seller";

        ResultSet resultSet = statement.executeQuery(script);

        while (resultSet.next()) {

            Seller seller1 = new Seller();
            seller1.getId(resultSet.getString(1));
            seller1.getSellerFirstName(resultSet.getString(2));
            seller1.getSellerLastName(resultSet.getString(3));
            seller1.getSellerPhoneNumber(resultSet.getString(4));
            seller.add(seller1);
        }
        return seller;
    }
}