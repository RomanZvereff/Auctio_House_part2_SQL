CREATE DATABASE IF NOT EXISTS auction;

USE auction;

create table seller (
	id int(11) auto_increment primary key,
	seller_first_name varchar(10),
	seller_last_name varchar(10),
	seller_phone_number numeric(12)
);

create table product (
	id int(11) auto_increment primary key,
	product_name varchar(20),
	product_type varchar(30),
	product_address varchar(50),
	product_start_price numeric(10, 2),
	product_date_of_sale date,
	product_status varchar(2)
);

create table buyer (
	id int(11)auto_increment primary key,
	buyer_first_name varchar(10),
	buyer_last_name varchar(10),
	buyer_phone numeric(12),
	buyer_bid numeric(10, 2)
);

create table bid (
	id int(11)auto_increment primary key,
	bid_size int(10)
);

alter table product 
	add column fk_seller_id int (11);
alter table product 
	add constraint foreign key (fk_seller_id) references seller(id);
    
alter table bid
	add column fk_product_id int(11);
alter table bid
	add constraint foreign key (fk_product_id) references product(id);
	
alter table bid
	add column fk_buyer_id int(11);
alter table bid
	add constraint foreign key (fk_buyer_id) references buyer(id);
    
insert into seller 
			(seller_first_name, 
			 seller_last_name, 
			 seller_phone_number) 
	values 	('John', 'Smith', 555123789),
			('Mark', 'Twen', 789236145),
            ('Ann', 'Qwerty', 321654987),
            ('Ed', 'Sheeran', 587230025),
            ('Emma', 'Hearts', 854962785);

insert into product 
			(product_name,
            product_address,
            product_start_price,
            product_date_of_sale,
            product_status,
            fk_seller_id)
	values 	('House', '5322 Otter Lane Middleberge FL 32068', 1000, 20171007, 'O', 1),
			('Office',	'1807 Glenwood St. NE Palm Bay FL 32907', 1200, 20170901, 'O', 2),
            ('Penthouse', 'P.O. Box 1230 Georgetown Cayman Islands B. W. I.', 2000, 20171105, 'O', 3),
            ('Flat', 'Dhahran 31311 Saudi Arabia', 500, 20171215, 'O', 4),
            ('Apartment', '6762 33 Ave N St Petersburg FL 33710', 1800, 20170123, 'O', 5);

insert into buyer 
			(buyer_first_name,
             buyer_last_name,
             buyer_phone)
	values 	('Rose', 'Chester', 555741369),
			('Max',	'Glenwood', 555753021),
            ('Anna', 'Cayman', 896321478),
            ('Sam', 'Dhahran', 320002145),
            ('Patricia', 'Av', 258369147),
            ('Ron', 'Potter', 852963741),
            ('Monica', 'Smith', 123456789),
            ('Joye', 'Petek', 159753789),
            ('Tim', 'Koock', 159025301),
            ('Bill', 'Trump', 555777999),
            ('Melisa', 'Kartner', 555250011);
            
insert into bid
			(bid_size,
            fk_product_id,
            fk_buyer_id)
	values  (1050, 1, 1),
			(1300, 2, 2),
            (2200, 3, 3),
            (550, 4, 4),
            (2000, 5, 5),
            (1150, 1, 6),
            (2500 , 3, 7),
            (2350 , 5 ,8),
            (750 ,4 , 9),
            (1300 , 1, 10),
            (1700, 1, 11),
            (1550 , 1 ,5),
            (1800 , 2, 3);
            
select  
	t1.seller_first_name,
	t1.seller_last_name,
    t1.seller_phone_number,
    t2.product_name,
    t2.product_address,
    t2.product_start_price,
    t2.product_date_of_sale,
    t2.product_status,
    t4.buyer_first_name,
    t4.buyer_last_name,
    t4.buyer_phone,
    t3.bid_size        
		from seller as t1
			inner join product as t2 on t1.id = t2.fk_seller_id
			inner join bid as t3 on t2.id = t3.fk_product_id
			inner join buyer as t4 on t4.id = t3.fk_buyer_id
    order by product_start_price, bid_size;